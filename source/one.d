module one;
import std.string;

// Capture the return type with logs
struct NumberWithLogs {
    int result;
    const string[] logs;
}

NumberWithLogs square(int x) {
    return NumberWithLogs(x * x, [format("Squared %d to get %d", x, x * x)]);
}

NumberWithLogs addOne(const NumberWithLogs num) {
    return NumberWithLogs(num.result + 1, num.logs ~ format("Add 1 to %d", num.result));
}