module zero;

// Initial intuition

int square(int x) {
    return x * x;
}

int addOne(int x) {
    return x + 1;
}