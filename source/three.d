module three;
import std.string;

// Capture the return type with logs
struct NumberWithLogs {
    int result;
    const string[] logs;
}

// Like a constructor, helps int becomes NumberWithLogs
NumberWithLogs wrapWithLogs(int x) {
    return NumberWithLogs(x, []);
}

// Introduce run function
NumberWithLogs runWithLogs(const NumberWithLogs input, NumberWithLogs function(int) transform){
    const NumberWithLogs newResult = transform(input.result);
    return NumberWithLogs(newResult.result, input.logs ~ newResult.logs);
}

// Rewrote square from two
NumberWithLogs square_reorganized(const NumberWithLogs num) {
    int x = num.result;
    const NumberWithLogs newNumberWithLogs = NumberWithLogs(
        x * x, 
        [format("Squared %d to get %d", x, x * x)]
    );

    // Notice similarity with run function
    return NumberWithLogs(newNumberWithLogs.result, num.logs ~ newNumberWithLogs.logs);
}

// Now our functions can take in int and return NumberWithLogs
NumberWithLogs square(int x) {
    return NumberWithLogs(
        x * x, 
        [format("Squared %d to get %d", x, x * x)]);
}

NumberWithLogs addOne(int x) {
    return NumberWithLogs(
        x + 1,
        [format("Add 1 to %d", x)]);
}