import std.stdio;

void main()
{
	// Starting Code
	{
		import zero : addOne, square;
		// addOne(square(2)) => 5
		writeln("Initial");
		writeln(addOne(square(2)));

		// However let's say we wanted to add logging to the results
		// to look something like this
		// addOne(square(2)) => 
		// {
		//	res: 5, 
		//  logs: [
		//	"Squared 2", 
		//  "Add 1 to 4"
		// ]
		// }
	}

	{
		import one : addOne, square;
		// Approach 1, use a struct to capture the return type 
		writeln("Approach 1");
		writeln(addOne(square(2)));

		// However we cannot do the following
		// square(square(2));
		// square(addOne(2));
		// addOne(2);
	}

	{
		import two : addOne, square, wrapWithLogs;
		// Introduce new function wrapWithLogs, like a constructor of sorts
		// Approach 2, now have a wrapper function

		writeln("Approach 2");
		writeln(square(square(wrapWithLogs(2))));
		writeln(addOne(addOne(wrapWithLogs(2))));

		writeln(square(addOne(wrapWithLogs(2))));
		writeln(addOne(square(wrapWithLogs(2))));

		writeln(addOne(wrapWithLogs(2)));
		writeln(square(wrapWithLogs(2)));

	}

	{
		import three : addOne, runWithLogs, square, wrapWithLogs;
		// It seems to have duplicated logic, log concat
		// Approach 3, introduce a run function
		// oldStyle: two.addOne(two.wrapWithLog(5));
		// newStyle: three.runWithLogs(three.wrapWithLogs(5), three.addOne);

		writeln("Approach 3");
		writeln(runWithLogs(wrapWithLogs(5), &addOne));
		writeln(runWithLogs(wrapWithLogs(5), &square));

		writeln(runWithLogs(runWithLogs(wrapWithLogs(5), &addOne), &square));
		writeln(runWithLogs(runWithLogs(wrapWithLogs(5), &square), &addOne));
	}
}