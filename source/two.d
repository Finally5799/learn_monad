module two;
import std.string;

// Capture the return type with logs
struct NumberWithLogs {
    int result;
    const string[] logs;
}

// Like a constructor, helps int becomes NumberWithLogs
NumberWithLogs wrapWithLogs(int x) {
    return NumberWithLogs(x, []);
}

// Now our functions can take in NumberWithLogs and return NumberWithLogs
NumberWithLogs square(const NumberWithLogs num) {
    int x = num.result;
    return NumberWithLogs(
        x * x, 
        num.logs ~ format("Squared %d to get %d", x, x * x)
    );
}

NumberWithLogs addOne(const NumberWithLogs num) {
    return NumberWithLogs(
        num.result + 1, 
        num.logs ~ format("Add 1 to %d", num.result)
    );
}